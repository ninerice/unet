import argparse
import cv2
import errno
import os
from model import *
from data import *
from keras.callbacks import TensorBoard
from time import time

#os.environ["CUDA_VISIBLE_DEVICES"] = "0"
def mkdir_p(path):
    try:
        os.makedirs(path)
    except OSError as exc:  # Python >2.5
        if exc.errno == errno.EEXIST and os.path.isdir(path):
            pass
        else:
            raise

def train(args):
    target_size = args.target_size
    data_gen_args = dict(rotation_range=0.2,
                         width_shift_range=0.05,
                         height_shift_range=0.05,
                         shear_range=0.05,
                         zoom_range=0.05,
                         horizontal_flip=True,
                         fill_mode='nearest')
    validation_split = 0.1
    trGene = trainGenerator(2, args.train_dir,
                            'image',
                            'label',
                            data_gen_args,save_to_dir = None,
                            target_size=(args.target_size,args.target_size),
                            validation_split=validation_split)
    vaGene = trainGenerator(2, args.train_dir,
                            'image',
                            'label',
                            data_gen_args,save_to_dir = None,
                            target_size=(args.target_size,args.target_size),
                            validation_split=-validation_split)

    model = unet(input_size=(args.target_size,args.target_size,1))
    model_checkpoint = ModelCheckpoint(args.checkpoint_file,
                                       monitor='loss',verbose=1,
                                       save_best_only=True)
    tensorboard = TensorBoard(log_dir="logs/{}".format(time()))
    model.fit_generator(trGene,
                        steps_per_epoch=args.steps_per_epoch,
                        epochs=args.epochs,
                        validation_data=vaGene,
                        validation_steps=int(args.steps_per_epoch*validation_split),
                        callbacks=[model_checkpoint,tensorboard])

def test(args):
    model = unet(input_size=(args.target_size,args.target_size,1))
    model.load_weights(args.checkpoint_file)
    test_steps = 30
    evalGene = trainGenerator(2, args.eval_dir,
                              'image',
                              'label',
                              dict(),
                              target_size=(args.target_size,args.target_size))
    loss, acc = model.evaluate_generator(evalGene, test_steps, verbose=1)
    print("test loss is {}, acc is {}".format(loss, acc))
    testGene = testGenerator(args.test_dir,
                             target_size = (args.target_size,args.target_size))
    results = model.predict_generator(testGene, test_steps, verbose=1)
    mkdir_p(args.test_result_dir)
    saveResult(args.test_result_dir, results)

def inference(args):
    model = unet(input_size=(args.target_size,args.target_size,1))
    model.load_weights(args.checkpoint_file)

    image = cv2.imread(args.inference_src)
    output = image.copy()
    target_size = args.target_size
    src_shape = (target_size, target_size)
    image = cv2.resize(image, src_shape)
    image = image.astype("float") / 255.0
    image = (0.21 * image[:,:,:1]) + (0.72 * image[:,:,1:2]) + (0.07 * image[:,:,-1:])
    image = image.reshape(1, image.shape[0], image.shape[1], image.shape[2])
    preds = model.predict(image)
    preds = preds * 255
    if (args.inference_dest):
        out = args.inference_dest
    else:
        filename, file_extension = os.path.splitext(args.inference_src)
        out = filename + '-pred' + file_extension

    cv2.imwrite(out, preds.reshape(preds.shape[1], preds.shape[2], preds.shape[3]))
    print("inference result for {} saved in {}".format(args.inference_src,
                                                       args.inference_dest))
    pass

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='unet')
    parser.add_argument('action', type=str,
                        default='train',
                        choices=['train', 'test', 'inference']);
    parser.add_argument('-c', '--checkpoint-file', type=str,
                        default='unet_membrane.hdf5');
    parser.add_argument('-t', '--train-dir', type=str,
                        default='data/membrane/train');
    parser.add_argument('--eval-dir', type=str,
                        default='data/membrane/eval');
    parser.add_argument('-v', '--validation-dir', type=str,
                        default='data/membrane/validation');
    parser.add_argument('--test-dir', type=str,
                        default='data/membrane/test');
    parser.add_argument('-r', '--test-result-dir', type=str,
                        default='data/membrane/test_result');
    parser.add_argument('--steps-per-epoch', type=int,
                        default=300);
    parser.add_argument('-e', '--epochs', type=int,
                        default=10);
    parser.add_argument('--target-size', type=int,
                        default=256);
    parser.add_argument('-i', '--inference-src', type=str,
                        default='');
    parser.add_argument('-d', '--inference-dest', type=str,
                        default='');
    args = parser.parse_args()

    if args.action == 'train':
        train(args)
    elif args.action == 'test':
        test(args)
    elif args.action == 'inference':
        inference(args)
    else:
        pass
    
